# Wellcome to HIGH-LOW Game!

You need to guess the number between the given range.

run the program (Linux)

``` bash
./bin/main.out
```

build & run

``` bash
make run
```

**DEBUG:** 

.vscode/launch.json change the executable extension to your preferred os. 

**Linux:** .out

**Windows**: .exe

**Mac**: .app

``` json
# .vscode/launch.json
  ...
  "program": "${workspaceFolder}/bin/${fileBasenameNoExtension}.out",
  ...
```

@VSCode, while your file.cpp tab active, press F5.

