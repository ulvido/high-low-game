#include "player.hpp"

using std::string;

// CONSTRUCTORS
Player::Player() : _name("NO_NAME") {}       // isim vermezse
Player::Player(string name) : _name(name) {} // isim verirse (overload)

// DESTRUCTOR
Player::~Player() {}

// SETTERS
void Player::setName(string name) { _name = name; }

// GETTERS
string Player::getName() { return _name; }