#pragma once
#include <string>

using std::string;

class Player
{
public:
  // CONSTRUCTORS
  Player();
  Player(string name); // overload
  // DESTRUCTOR
  ~Player();
  // SETTERS
  void setName(string name);
  // GETTERS
  string getName();

private:
  string _name;
};