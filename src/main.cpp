#include "game.hpp"
#include "player.hpp"
#include <iterator>
#include <iostream>
#include <string>

using std::advance;
using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
  Game g; // create game

  // create players
  Player p1; // NO_NAME
  Player p2("OSMAN");
  Player p3("Ulvi");

  // players joined game
  g.addPlayer(p1);
  g.addPlayer(p2);
  g.addPlayer(p3);

  // player 1 adını değiştirdi
  p1.setName("Özge");
  // player 2 quiter
  g.removePlayer(p2);

  // log players
  list<Player *> l = g.getPlayers();
  list<Player *>::iterator it;
  for (it = l.begin(); it != l.end(); it++)
  {
    cout << (*it)->getName() << endl;
  }

  // oyun ayarları
  g.setMini(0);
  g.setMaxi(10);

  // oyunu başlat
  g.startGame();
  // cout << "GOAL: " << g.getGoal() << endl;

  // DEMO - gerçek oyun turn bazlı değil realtime olcak
  string userInput;
  int userNumber;
  list<Player *> players = g.getPlayers();        // bunu bir sefer çağırmak lazım yoksa kafası karışıyor.
  list<Player *>::iterator pit = players.begin(); // daha önceden 1 kez çağırdığımız player için iterator
  do
  {
    cout << (*pit)->getName() << " enter a number: ";
    getline(cin, userInput);      // get whole line in one
    userNumber = stoi(userInput); // convert to int
    cout << g.makeGuess(*(*pit), userNumber) << endl;

    pit++; // sıradaki oyuncuya geç
    // sona geldiyse başa dönsün
    if (pit == players.end())
    {
      pit = prev(pit, players.size());
    }

  } while (g.isGameOver() != true);
  // // tahmin yap
  // cout << p1.getName() << " : " << g.makeGuess(p1, 1) << endl; //abuzittin 1 dedi
  // // string kazanan = g.getWinner() ? g.getWinner()->getName() : "YOK";
  // cout << p3.getName() << " : " << g.makeGuess(p3, 2) << endl; //mahmıt 2 dedi

  // kazanan
  string kazanan = g.getWinner() ? g.getWinner()->getName() : "YOK";
  cout << "KAZANAN: " << kazanan << endl;

  return 0;
}