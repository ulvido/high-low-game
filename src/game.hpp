#pragma once
#include "player.hpp"
#include <vector>
#include <list>
#include <map>

using std::list;
using std::map;
using std::vector;

class Game
{
public:
  // CONSTRUCTOR
  Game();
  // DESTRUCTOR
  ~Game();
  // SETTERS
  void setMini(int mini);
  void setMaxi(int maxi);
  void setGoal(int goal); // hedef
  // GETTERS
  int getMini();
  int getMaxi();
  int getGoal();
  list<Player *> getPlayers();
  bool isGameOver();
  Player *getWinner();
  // METHODS
  void addPlayer(Player &p);    // join game
  void removePlayer(Player &p); // leave game
  void startGame();
  string makeGuess(Player &p, int guess); // tahminlere ekler ve sonuç dönderir ( HIGH, LOW, WIN)
  void endGame();

private:
  // GETTER - SETTER
  int _mini,
      _maxi,
      _goal;
  // BG ASSIGNMENT
  list<Player *> _players;           // oyuncu listesi
  map<Player, vector<int>> _guesses; // tahminler. map of vectors {(Player1, {int}),(Player2, int),(Player1, int),...}
  bool _isGameOver = false;          // game loop breaker
  Player *_winner = nullptr;         // kazanan oyuncu
};