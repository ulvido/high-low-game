#include "game.hpp"
#include <cstdlib>
#include <ctime>

// CONSTRUCTOR
Game::Game() {}
// DESTRUCTOR
Game::~Game() {}

// SETTERS
void Game::setMini(int mini) { _mini = mini; };
void Game::setMaxi(int maxi) { _maxi = maxi; };
void Game::setGoal(int goal) { _goal = goal; }; // hedef

// GETTERS
int Game::getMini() { return _mini; };
int Game::getMaxi() { return _maxi; };
int Game::getGoal() { return _goal; };
list<Player *> Game::getPlayers() { return _players; };
bool Game::isGameOver() { return _isGameOver; };
Player *Game::getWinner() { return _winner; };

// METHODS

// join game
void Game::addPlayer(Player &p)
{
  _players.push_back(&p);
};

// leave game
void Game::removePlayer(Player &p)
{
  _players.remove(&p);
};

void Game::startGame()
{
  _isGameOver = false; // oyunu sıfırla
  // create goal between mini and maxi
  srand(time(0));                                     // use time to create true random number
  _goal = (_mini + ((rand() % (_maxi - _mini)) + 1)); // create random number
};

// tahmin yap
string Game::makeGuess(Player &p, int guess)
{
  // tahmini yapan playerın tahminlerine tahmini ekle
  // _guesses[p].push_back(guess);

  // tahmini işle
  if (guess < _goal) // input smaller than goal
  {
    return "LOW";
  }
  else if (guess > _goal) // input bigger than goal
  {
    return "HIGH";
  }
  else if (guess == _goal) // GOAL
  {
    _winner = &p;
    _isGameOver = true;
    return "WIN";
  }
  else if (guess < _mini || guess > _maxi) // input beyond mini - maxi
  {
    return "INPUT BEYOND LIMIT";
  }
  else // error
  {
    return "UNEXPECTED ERROR ON GUESS!";
  }
};

// oyunu bitir
void Game::endGame(){};